let s:root = expand('<sfile>:h:h:h')

function! remote#handler(channel, cmd)
  let cmd = substitute(a:cmd, '\v\r\n', '', 'g')
  if (cmd == '')
    return
  endif

  execute cmd
endfunction

function! remote#nvim_handler(channel, cmd, ...)
    let cmd = a:cmd[0]
    call remote#handler(a:channel, cmd)
endfunction

function! remote#start()
  if (!has('nvim'))
      call job_start(['node', s:root . '/vim-remote/index.js'], {"out_cb": "remote#handler"})
  else
      call jobstart(['node', s:root . '/vim-remote/index.js'], {"on_stdout": "remote#nvim_handler"})
  endif
endfunction
