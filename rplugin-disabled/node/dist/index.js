"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const net_1 = require("net");
// 
// @Plugin({dev: true})
// class MyTestPlugin {
//     constructor(private _nvim: Neovim, private _plugin: NvimPlugin) {
//         console.log('params are', this._nvim, this._plugin);
//     }
//     @Command('LongCommand')
//     private async longCommand(args) {
//         await this._nvim.outWriteLine("Hey there");
//     }
// 
//     // @Autocmd('BufEnter', {pattern: '*.js'})
//     // private _bufEnter(args) {
//     //     this._nvim.eval('echomsg "HERE WE ARE"');
//     // }
// 
// }
class RemoteCommands {
    constructor(_plugin) {
        this._plugin = _plugin;
        this._plugin.setOptions({ dev: false, alwaysInit: false });
        console.log('hello world');
        net_1.createServer(socket => {
            socket.on('data', data => this._plugin.nvim.eval(data.toString()));
        }).listen(8001);
    }
}
module.exports = RemoteCommands;
//# sourceMappingURL=index.js.map
